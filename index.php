<?php

$search_words = array('php','html','интернет','Web');

$search_strings = array(

'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',

'PHP - это распространенный язык программирования с открытым исходным кодом.',

'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'

);

function search($search_words, $search_strings)
{
  $result = [];

  foreach ($search_words as $word)
  {
    $patern = '/.*'.$word.'.*/iu';

    foreach ($search_strings as $key => $string)
    {

      if (preg_match($patern,$string))
      {
        $result[$key][] =$word;
      }
    }
  }

  ksort($result);

  foreach ($result as $key => $string)
  {
    $key = $key+1;
    echo 'Предложение №'.$key.' содержит слово: '.implode(', ', $string).'<br>';
  }

}

search($search_words, $search_strings);